
public class Validator {

	public static boolean validDef(String line) {
		if (line.length() == 0)
			return false;
		if (line.charAt(0) == '#')
			return false;
		if (line.indexOf('=') == -1)
			return false;
		if (line.indexOf('=') != line.lastIndexOf('='))
			return false;
		return true;
	}
	
	public static boolean validFileName(String name) {
		for (int i = 0; i < name.length(); i++) {
			char c = name.charAt(i);
			if (!Character.isLetterOrDigit(c))
				return false;
		}
		return true;
	}
	
	public static boolean validLokumText(String text) {
		if (text.length() == 0)
			return false;
		
		if (text.length() == 1){
			return LokumGG.types.get(text).equals("ColorBombLokum");
		}else {
			return ((LokumGG.types.containsKey(""+text.charAt(0)))
					&& (LokumGG.colors.containsKey(""+text.charAt(1)))
					&& (text.length() > 2 ? 
							(LokumGG.details.get(LokumGG.types.get(""+text.charAt(0))) == null ?
									false :
									(LokumGG.details.get(LokumGG.types.get(""+text.charAt(0))).containsKey("%") ?
											validNumber(text.substring(2)) :
											LokumGG.details.get(LokumGG.types.get(""+text.charAt(0))).containsKey(text.substring(2))))
							: true)
					);
		}
		
	}
	
	public static boolean validNumber(String number) {
		for (int i = 0; i < number.length(); i++) {
			char c = number.charAt(i);
			if (!Character.isDigit(c))
				return false;
		}
		return true;
	}
}
