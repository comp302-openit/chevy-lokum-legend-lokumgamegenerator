
public class InvalidLokumGameFileException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cause;
	
	public InvalidLokumGameFileException(String cause) {
		this.cause = cause;
	}
	
	public String cause(){
		return cause;
	}

}
