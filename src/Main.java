
public class Main {

	public static void main(String[] args) {
		if (args.length > 0) {
			LokumGG.open(args[0]);
		} else {
			LokumGG.open(LokumGG.DEFAULT_MODE);
		}
	}	
}
