public class GUIController {
	
	private static GUI frame;
	
	public static void createGUI() {
		frame = new GUI();
		frame.setVisible(true);
	}
	
	public static void displayMessage(String msg) {
		if (frame == null) {
			GUI.showErrorWindow(msg);
		} else {
			frame.displayMessage(msg);
		}
	}

}
