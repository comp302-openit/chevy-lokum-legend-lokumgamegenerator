import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel boardGrid;
	private JPanel boardArea;
	
	private JTextArea textArea;
	
	private JTextField[][] textBoard;
	private JTextField movesLeftText;
	private JTextField scoreText;
	private JTextField specialSwapText;
	private JTextField levelText;
	private JTextField savenameText;
	
	
	private JFrame messageBox;
	
	private int winW = (40+Integer.parseInt(LokumGG.conf.get("board-width"))*40 > 400 ? 40+Integer.parseInt(LokumGG.conf.get("board-width"))*40 : 400);
	private int winH = (300+Integer.parseInt(LokumGG.conf.get("board-height"))*30 > 350 ? 300+Integer.parseInt(LokumGG.conf.get("board-height"))*30 : 350);
	
	private void init() {
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.X_AXIS));
		this.setSize(winW,winH);
		this.setLocationRelativeTo(null);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}	
	
	public GUI() {
		super();
		init();
		JPanel container = new JPanel();
		container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
		
		// header
		JPanel headerPanel = new JPanel();
		headerPanel.setLayout(new GridLayout(2,1));
		headerPanel.setMaximumSize(new Dimension(winW,100));
		JLabel header = new JLabel("LokumGG", JLabel.CENTER);
		header.setFont(header.getFont().deriveFont(20F));
		headerPanel.add(header);
		JLabel info = new JLabel("<html><center>"
				+ "OpenIT presents the game generator<br>"
				+ "for Chevy Lokum Legend,<br>"
				+ "another COMP302 project"
				+ "<center></html>",
				JLabel.CENTER);
		info.setFont(info.getFont().deriveFont(10F));
		headerPanel.add(info);
		container.add(headerPanel);
		
		
		container.add(Box.createVerticalStrut(20));
		
		// save fields
		JPanel savePathfield = new JPanel();
		savePathfield.setLayout(new BoxLayout(savePathfield, BoxLayout.X_AXIS));
		savePathfield.setMaximumSize(new Dimension(winW,60));
		savePathfield.add(new JLabel("Game Path : " + LokumGG.conf.get("project-path")));
		container.add(savePathfield);
		JPanel saveInfofield = new JPanel();
		saveInfofield.setLayout(new BoxLayout(saveInfofield, BoxLayout.X_AXIS));
		saveInfofield.setMaximumSize(new Dimension(winW,60));
		saveInfofield.add(new JLabel("Player Name : " + LokumGame.playername));
		container.add(saveInfofield);
		JPanel savenamefield = new JPanel();
		savenamefield.setLayout(new BoxLayout(savenamefield, BoxLayout.X_AXIS));
		savenamefield.setMaximumSize(new Dimension(winW,60));
		savenamefield.add(new JLabel("Save Name : "));
		savenameText = new JTextField(LokumGG.conf.get("default-save-name"));
		savenamefield.add(savenameText);
		savenamefield.add(new JLabel(".xml"));
		container.add(savenamefield);
		
		container.add(Box.createVerticalStrut(20));
		
		JPanel optionPanel = new JPanel();
		optionPanel.setLayout(new BoxLayout(optionPanel, BoxLayout.X_AXIS));
		optionPanel.add(new JLabel("Input Method : "));
		ButtonGroup bg = new ButtonGroup();
		JRadioButton grid = new JRadioButton("Grid");
		grid.setSelected(true);
		grid.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(!boardGrid.isVisible())
					toggleInputMethod();
			}
			
		});
		JRadioButton area = new JRadioButton("Area");
		area.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(!boardArea.isVisible())
					toggleInputMethod();
			}
			
		});
		bg.add(grid);
		bg.add(area);
		optionPanel.add(grid);
		optionPanel.add(area);
		container.add(optionPanel);
				

		container.add(Box.createVerticalStrut(20));
		
		// board

		
		boardGrid = new JPanel();
		textBoard = new JTextField[Integer.parseInt(LokumGG.conf.get("board-height"))][Integer.parseInt(LokumGG.conf.get("board-width"))];
		boardGrid.setLayout(new GridLayout(textBoard[0].length+1,textBoard.length+1));
		boardGrid.setMaximumSize(new Dimension(winW-40,20));
		for (int y = -1; y < textBoard.length; y++) {
        	for (int x = -1; x < textBoard[0].length; x++) {
        		JComponent cmp;
        		if(x>=0 && y>=0){
        			JTextField field = new JTextField();
            		field.setText(LokumGG.getRandomLokumString());
            		field.setHorizontalAlignment(JTextField.CENTER);
            		field.setMinimumSize(new Dimension(30, 30));
            		textBoard[y][x] = field;
            		cmp = field;
        		} else {
        			if (x == -1 && y == -1) {
        				cmp = new JLabel();
        			} else {
        				if (x >=0) {
        					JLabel l = new JLabel(x+"");
        					l.setHorizontalAlignment(JLabel.CENTER);
        					l.setVerticalAlignment(JLabel.CENTER);
        					cmp = l;
        				} else {
        					JLabel l = new JLabel(y+"");
        					l.setHorizontalAlignment(JLabel.CENTER);
        					l.setVerticalAlignment(JLabel.CENTER);
        					cmp = l;
        				}
        			}
        		}
        		boardGrid.add(cmp);
        	}
        }
		container.add(boardGrid);
		
		boardArea = new JPanel();
		boardArea.setLayout(new GridLayout(1,1));
		textArea = new JTextArea(textBoard[0].length*5, textBoard.length);
		boardArea.add(textArea);
		boardArea.setVisible(false);
		container.add(boardArea);
		
		container.add(Box.createVerticalStrut(20));
		
		// fields panel
		JPanel fields = new JPanel();
		fields.setLayout(new GridLayout(2,4));
		fields.setMaximumSize(new Dimension(winW-40,60));
		fields.add(new JLabel("Moves Left"));
		fields.add(new JLabel("Score"));
		fields.add(new JLabel("Special Swap Left"));
		fields.add(new JLabel("Level"));
		movesLeftText = new JTextField(LokumGG.conf.get("default-moves-left"));
		fields.add(movesLeftText);
		scoreText = new JTextField(LokumGG.conf.get("default-score"));
		fields.add(scoreText);
		specialSwapText = new JTextField(LokumGG.conf.get("default-special-swap-left"));
		fields.add(specialSwapText);
		levelText = new JTextField(LokumGG.conf.get("default-level"));
		fields.add(levelText);
		container.add(fields);

		container.add(Box.createVerticalStrut(20));


		// buttons panel
		JPanel buttons = new JPanel();
		JButton syntaxButton = new JButton("Syntax?");
		syntaxButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String table = "<html><center>"
						+ "<p>Syntax: \"[type]\" or \"[type][color]\" or \"[color][type][additional]\"</p>"
						+ "<br>"
						+ "<table border=1><tr>"
						+ "<td>Color</td><td>Type</td><td>Additional</td>"
						+ "</tr><tr>";
				table += "<td>"+generateLegendText(LokumGG.types)+"</td>";
				table += "<td>"+generateLegendText(LokumGG.colors)+"</td>";
				table += "<td>"+generateDetailsLegendText(LokumGG.details)+"</td>";
				table += "</tr></table>"
						+ "<p>'%' is any non-negative integer</p></center></html>";
				displayMessage(table);
			}
			
		});
		buttons.add(syntaxButton);
		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (boardArea.isVisible()){
					setGrid();
				}
				String errors = checkFields();
				if (errors.length() == 0) {
					LokumGame game = new LokumGame(
							Lokum.getBoardLokums(fieldsToStrings()),
							Integer.parseInt(movesLeftText.getText()),
							Double.parseDouble(scoreText.getText()),
							Integer.parseInt(levelText.getText()),
							Integer.parseInt(specialSwapText.getText()),
							savenameText.getText()
							);
					LokumGG.save(game);
					displayMessage("Saved");
				} else {
					displayMessage(errors);
				}
			}
			
		});
		buttons.add(saveButton);
		JButton exitButton = new JButton("Exit");
		exitButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);				
			}
			
		});
		buttons.add(exitButton);
		container.add(buttons);
		
		this.getContentPane().add(Box.createHorizontalStrut(20));
		this.getContentPane().add(container);
		this.getContentPane().add(Box.createHorizontalStrut(20));
		
		pack();
		this.setVisible(true);
	}
	
	private void toggleInputMethod() {
		boolean grid = boardGrid.isVisible();
		boolean area = boardArea.isVisible();
		
		if (grid) setArea();
		if (area) setGrid();
		
		boardGrid.setVisible(!grid);
		boardArea.setVisible(!area);
	}

	private String[][] fieldsToStrings() {
		String[][] result = new String[textBoard.length][textBoard[0].length];
		for(int y = 0; y < textBoard.length; y++) {
			for(int x = 0; x < textBoard[0].length; x++) {
				result[y][x] = textBoard[y][x].getText().trim();
			}
		}
		return result;
	}
	
	private String generateLegendText(HashMap<String, String> map){
		String result = "<table border=0>";
		for (String s : map.keySet()) {
			result += "<tr><td>"+s+"</td><td>=</td><td>"+map.get(s)+"</td></tr>";
		}
		result += "</table>";
		return result;
	}
	
	private String generateDetailsLegendText(HashMap<String, HashMap<String,String>> map){
		String result = "<table border=0>";
		for (String s : map.keySet()) {
			for(String m : map.get(s).keySet()){
				result += "<tr><td>"+s+"</td><td>"+m+"</td><td>=</td><td>"+map.get(s).get(m)+"</td></tr>";
			}
		}
		result += "</table>";
		return result;
	}
	
	public void displayMessage(String msg) {
		// messageBox appears
		final JFrame gui = this;
		
		messageBox = new JFrame();
		messageBox.setLayout(new BoxLayout(messageBox.getContentPane(), BoxLayout.Y_AXIS));
		messageBox.setLocationRelativeTo(null);
		messageBox.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				messageBox.dispose();
				gui.setEnabled(true);
			}
		});
		
		JPanel messagePanel = new JPanel();
		JLabel message = new JLabel(processMessage(msg));
		message.setFont(message.getFont().deriveFont(10F));
		messagePanel.add(message);
		messageBox.getContentPane().add(messagePanel);
		
		JPanel buttonsPanel = new JPanel();
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				messageBox.dispose();
				gui.setEnabled(true);
			}
			
		});
		buttonsPanel.add(okButton);
		messageBox.getContentPane().add(buttonsPanel);
		
		gui.setEnabled(false);
		messageBox.pack();
		messageBox.setVisible(true);
	}
	
	private String checkFields() {
		String errors = "";
		
		// check filename
		if (!Validator.validFileName(savenameText.getText())){
			errors += "Error: Invalid file name.";
		}
		
		// check lokum codes
		boolean erroneousLokum = false;
		for(int y = 0; y < textBoard.length; y++) {
			for(int x = 0; x < textBoard[0].length; x++) {
				if (!Validator.validLokumText(textBoard[y][x].getText().trim())){
					errors += "Error: Invalid lokum code.";
					erroneousLokum = true;
					break;
				}
			}
			if (erroneousLokum) break;
		}

		// check moves
		if (!Validator.validNumber(movesLeftText.getText())){
			errors += "Error: Invalid value for moves left.";
		}
		
		// check score
		if (!Validator.validNumber(scoreText.getText())){
			errors += "Error: Invalid value for score.";
		}

		// check special swaps
		if (!Validator.validNumber(specialSwapText.getText())){
			errors += "Error: Invalid value for special swap left.";
		}
		
		// check level
		if (!Validator.validNumber(levelText.getText())){
			errors += "Error: Invalid value for level.";
		}

		return errors.length() == 0 ? "" : errors;
	}
	
	private static String processMessage(String msg) {
		if(msg.length() < 7)
			return msg;
		if (msg.substring(0,7).equals("Error: ")) {
			String[] errors = msg.split("Error: ");
			String html = "<html><ul>Error:";
			for (int i = 1; i < errors.length; i++) {
				html += "<li>"+errors[i]+"</li>";
			}
			html += "</ul></html>";
			return html;
		} else {
			return msg;
		}
	}
	
	public static void showErrorWindow(String msg) {
		JFrame errorWindow = new JFrame();
		errorWindow.setLayout(new BoxLayout(errorWindow.getContentPane(), BoxLayout.Y_AXIS));
		errorWindow.setLocationRelativeTo(null);
		errorWindow.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		JPanel messagePanel = new JPanel();
		JLabel message = new JLabel(processMessage(msg));
		message.setFont(message.getFont().deriveFont(11F));
		messagePanel.add(message);
		errorWindow.getContentPane().add(messagePanel);
		
		JPanel buttonsPanel = new JPanel();
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
			
		});
		buttonsPanel.add(okButton);
		errorWindow.getContentPane().add(buttonsPanel);
		
		errorWindow.pack();
		errorWindow.setVisible(true);
	}
	
	private void setArea() {
		String boardString = "";
		for(int y = 0; y < textBoard.length; y++) {
			for(int x = 0; x < textBoard[0].length; x++) {
				boardString += textBoard[y][x].getText().trim() + " ";
			}
			boardString = boardString.substring(0, boardString.length() - 1) + "\n";
		}
		textArea.setText(boardString);
	}
	
	private void setGrid() {
		String[][] boardToCopy = new String[textBoard.length][textBoard[0].length];
		String[] strBoard = textArea.getText().split("\n");
		if (strBoard.length != textBoard.length)
			return;
		for(int y = 0; y < (textBoard.length); y++){
			String[] row = strBoard[y].split(" +");
			if (row.length != textBoard[0].length)
				return;
			boardToCopy[y] = row;
		}
		for (int y = 0; y < textBoard.length; y++) {
        	for (int x = 0; x < textBoard[0].length; x++) {
        			textBoard[y][x].setText(boardToCopy[y][x]);
        	}
        }
	}
}
