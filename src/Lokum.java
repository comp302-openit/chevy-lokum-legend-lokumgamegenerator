
public class Lokum {

	private String color;
	private String type;
	private String details;
	
	public Lokum(String color, String type) {
		this.color = color;
		this.type = type;
		this.details = null;
	}
	
	public Lokum(String color, String type, String details) {
		this(color, type);
		this.details = details;
	}
	
	public String color() {
		return color;
	}
	
	public String type() {
		return type;
	}
	
	public String details() {
		return details;
	}
	
	public static Lokum convertLokum(String str) {
		if (str.length() == 0)
			return null;
		if(str.length() == 1) {
			return (LokumGG.types.get(str).equals("ColorBombLokum"))? new Lokum("",LokumGG.types.get(str)) : null;
		} else if(str.length() > 2) {
			return new Lokum(LokumGG.colors.get(""+str.charAt(1)), LokumGG.types.get(""+str.charAt(0)),
					LokumGG.details.get(LokumGG.types.get(""+str.charAt(0))).containsKey("%") ?
							LokumGG.details.get(LokumGG.types.get(""+str.charAt(0))).get("%").replace("%", str.substring(2)) :
							LokumGG.details.get(LokumGG.types.get(""+str.charAt(0))).get(str.substring(2)));
		} else {
			return new Lokum(LokumGG.colors.get(""+str.charAt(1)), LokumGG.types.get(""+str.charAt(0)));
		}
	}
	
	public static Lokum[][] getBoardLokums(String[][] textBoard) {
		Lokum[][] result = new Lokum[textBoard.length][textBoard[0].length];
		for(int y = 0; y < textBoard.length; y++) {
			for(int x = 0; x < textBoard[0].length; x++) {
				result[y][x] = convertLokum(textBoard[y][x]);
			}
		}
		return result;
	}
}
