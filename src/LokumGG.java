import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class LokumGG {
	
	public static boolean OK = true;
	
	public static final String GUI_MODE = "GUI_MODE"; 
	public static final String CONSOLE_MODE = "CONSOLE_MODE"; 
	public static final String DEFAULT_MODE = GUI_MODE;
	private static String mode = DEFAULT_MODE;
	
	private static final String confFile = "lgg.conf";
	private static final String lokumTypesFile = "types.lgg";
	private static final String lokumColorsFile = "colors.lgg";
	private static final String lokumDetailsFile = "details.lgg";
	
	public static HashMap<String, String> conf = readDef(confFile);
	public static HashMap<String, String> colors = readDef(lokumColorsFile);
	public static HashMap<String, String> types = readDef(lokumTypesFile);
	public static HashMap<String, HashMap<String, String>> details = readDetailsDef(lokumDetailsFile);
	
	public static void open(String m) {
		
		if (OK) {

			if (m.equals(GUI_MODE)) {
				GUIController.createGUI();
			} else {
				mode = CONSOLE_MODE;
				String fileName = m;
				if (!Validator.validFileName(fileName)) {
					displayMessage("Error: Invalid filename.");
				} else {
					try {
						save(new LokumGame(fileName));
					} catch (IOException e) {
						displayMessage("Error: File cannot be read: " + fileName);
						OK = false;
						return;
					} catch (InvalidLokumGameFileException e) {
						displayMessage("Error: "+e.cause());
						OK = false;
						return;
					}
				}
			}
			
		}
		
		
	}
	
	public static void save(LokumGame game) {
		 try {

		        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		        
		        TransformerFactory transformerFactory = TransformerFactory.newInstance();
		        Transformer transformer = transformerFactory.newTransformer();
		        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		        
		        String path = conf.get("project-path");
		        
		        // root elements
		        Document doc = docBuilder.newDocument();
		        Element rootElement = doc.createElement("game");
		        doc.appendChild(rootElement);

		        // player elements
		        Element player = doc.createElement("player");
		        rootElement.appendChild(player);
		        
		        // player id
		        Element id = doc.createElement("id");
		        id.appendChild(doc.createTextNode(game.playerid()));
		        player.appendChild(id);
		        // player name
		        Element name = doc.createElement("name");
		        name.appendChild(doc.createTextNode(game.playername()));
		        player.appendChild(name);
		        
		        // board elements
		        Element board = doc.createElement("board");
		        rootElement.appendChild(board);
		        Element lokums = doc.createElement("lokums");
		        Lokum[][] gameboard = game.board();        
		        for (int x = 0; x < gameboard[0].length; x++) {
		        	for (int y = 0; y < gameboard.length; y++) {
		        		Element lokum = doc.createElement("lokum");

		        		Element color = doc.createElement("color");
		        		color.appendChild(doc.createTextNode(gameboard[y][x].color()));
		        		lokum.appendChild(color);
				        
				        Element position = doc.createElement("position");
		        		Element xcoord = doc.createElement("xcoord");
				        xcoord.appendChild(doc.createTextNode(x+""));
				        position.appendChild(xcoord);
				        Element ycoord = doc.createElement("ycoord");
				        ycoord.appendChild(doc.createTextNode(y+""));
				        position.appendChild(ycoord);
				        lokum.appendChild(position);
				        
				        if (gameboard[y][x].details() != null) {
				        	String[] det = gameboard[y][x].details().split(":");
					        Element detail = doc.createElement(det[0]);
					        detail.appendChild(doc.createTextNode(det[1]));
					        lokum.appendChild(detail);
				        }
				        
				        Element type = doc.createElement("type");
				        type.appendChild(doc.createTextNode(gameboard[y][x].type()));
				        lokum.appendChild(type);
				               
				        lokums.appendChild(lokum);
		        	}
		        }
		        board.appendChild(lokums);
		        
		        // currentScore elements
		        Element score = doc.createElement("currentscore");
		        score.appendChild(doc.createTextNode(game.score()+""));
		        rootElement.appendChild(score);
		        
		        // movesLeft elements
		        Element moves = doc.createElement("movesleft");
		        moves.appendChild(doc.createTextNode(game.moves()+""));
		        rootElement.appendChild(moves);
		        
		        // level elements
		        Element level = doc.createElement("level");
		        level.appendChild(doc.createTextNode(game.level()+""));
		        rootElement.appendChild(level);
		        
		        // specialSwaps elements
		        Element specialSwaps = doc.createElement("specialswapsleft");
		        specialSwaps.appendChild(doc.createTextNode(game.specialSwap()+""));
		        rootElement.appendChild(specialSwaps);

		        // write the content into xml file
		        DOMSource source = new DOMSource(doc);
		        
		        String dir = path + (path.charAt(path.length()-1) != '/' ? "/" : "") + "xml/gameSaves/"+game.playername()+"/";
		        File f = new File(dir);
		        if(!f.exists()) f.mkdirs();
		        
		        StreamResult result = new StreamResult(new File(dir+game.savename()+".xml"));
		        
		        // Output to console for testing
		        //StreamResult result = new StreamResult(System.out);
		        
		        transformer.transform(source, result);
		        
		        /**
		         * 
		         */
		        
		        // root elements
		        Document pdoc = docBuilder.newDocument();
		        Element pRootElement = pdoc.createElement("session");
		        pdoc.appendChild(pRootElement);

		        // player elements
		        Element pplayer = pdoc.createElement("player");
		        pRootElement.appendChild(pplayer);
		        
		        // player id
		        Element pid = pdoc.createElement("id");
		        pid.appendChild(pdoc.createTextNode(game.playerid()));
		        pplayer.appendChild(pid);
		        // player name
		        Element pname = pdoc.createElement("name");
		        pname.appendChild(pdoc.createTextNode(game.playername()));
		        pplayer.appendChild(pname);
		        
		        // score
		        Element pscore = pdoc.createElement("score");
		        pscore.appendChild(pdoc.createTextNode("7777"));
		        pRootElement.appendChild(pscore);
		        
		        // maxlevel
		        Element pmaxlevel = pdoc.createElement("maxlevel");
		        pmaxlevel.appendChild(pdoc.createTextNode("1"));
		        pRootElement.appendChild(pmaxlevel);
		        
		        // write the content into xml file
		        DOMSource psource = new DOMSource(pdoc);
		        
		        String pdir = path + (path.charAt(path.length()-1) != '/' ? "/" : "") + "xml/sessionSaves/";
		        File pf = new File(pdir);
		        if(!pf.exists()) pf.mkdirs();
		        
		        StreamResult presult = new StreamResult(new File(pdir+game.playername()+".xml"));
		        
		        // Output to console for testing
		        //StreamResult presult = new StreamResult(System.out);
		        
		        transformer.transform(psource, presult);
		        
		        /**
		         * 
		         */

		      } catch (ParserConfigurationException pce) {
		    	  //pce.printStackTrace();
		    	  displayMessage("Error: Could not save.");
		      } catch (TransformerException tfe) {
		    	  //tfe.printStackTrace();
		    	  displayMessage("Error: Could not save.");
		      }
	}
	
	public static HashMap<String, String> readDef(String mapFile) {
		HashMap<String, String> map = new HashMap<String, String>();
		
		BufferedReader rd;
		try {
			FileReader f = new FileReader(mapFile);
			rd = new BufferedReader(f);
			String def = rd.readLine();
			while (def != null) {
				def = def.trim();
				if (Validator.validDef(def)) {
					String[] operands = def.split("=");
					map.put(operands[0].trim(), operands[1].trim());
				}
				def = rd.readLine();
			}
			rd.close();
		} catch (FileNotFoundException e) {
			displayMessage("File not found:" + mapFile);
			OK = false;
		} catch (IOException e) {
			displayMessage("File cannot be read: "+ mapFile);
			OK = false;
		}
		return map;
	}
	
	public static HashMap<String, HashMap<String, String>> readDetailsDef(String mapFile) {
		HashMap<String, HashMap<String,String>> map = new HashMap<String, HashMap<String,String>>();
		
		BufferedReader rd;
		try {
			rd = new BufferedReader(new FileReader(mapFile));
			String def = rd.readLine();
			while (def != null) {
				def = def.trim();
				if (Validator.validDef(def)) {
					String[] args = def.split(" ");
					HashMap<String, String> detailSet = map.get(args[0].trim());
					if (detailSet == null)
						detailSet = new HashMap<String, String>();
					String[] operands = args[1].split("=");
					detailSet.put(operands[0].trim(), operands[1].trim());
					map.put(args[0].trim(), detailSet);
				}
				def = rd.readLine();
			}
			rd.close();
		} catch (FileNotFoundException e) {
			displayMessage("File not found:" + mapFile);
			OK = false;
		} catch (IOException e) {
			displayMessage("File cannot be read: "+ mapFile);
			OK = false;
		}
		return map;
	}
	
	public static String getRandomLokumString() {
		return getKeyOf(types, conf.get("default-lokum-type")) + getRandomKey(colors);
	}
	
	private static String getRandomKey(HashMap<String,String> map) {
		ArrayList<String> keys = new ArrayList<String>(map.keySet());
		return keys.get((int) Math.floor(Math.random() * map.size()));
	}
	
	private static String getKeyOf(HashMap<String, String> map, String val) {
		for(String s : map.keySet()) {
			if (map.get(s).equals(val))
				return s;
		}
		return "not-found";
	}
	
	public static void displayMessage(String msg) {
		if (mode.equals(CONSOLE_MODE)) {
			System.out.println(msg);
		} else if (mode.equals(GUI_MODE)) {
			GUIController.displayMessage(msg);
		}	
	}
	
}