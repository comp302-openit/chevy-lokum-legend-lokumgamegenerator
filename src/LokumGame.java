import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class LokumGame {

	public static final String playerid = LokumGG.conf.get("default-player-id");
	public static final String playername = LokumGG.conf.get("default-player-name");
	
	public static final int defaultMovesLeft = Integer.parseInt(LokumGG.conf.get("default-moves-left"));
	public static final int defaultSpecialSwapLeft = Integer.parseInt(LokumGG.conf.get("default-special-swap-left"));
	public static final double defaultScore = Double.parseDouble(LokumGG.conf.get("default-score"));
	public static final int defaultLevel = Integer.parseInt(LokumGG.conf.get("default-level"));
	public static final String defaultSaveName = LokumGG.conf.get("default-save-name");
	
	private Lokum[][] board;
	private int movesLeft;
	private double score;	
	private int level;
	private int specialSwapLeft;
	private String savename;
	
	public LokumGame(Lokum[][] board, int movesLeft, double score, int level, int specialSwapLeft, String savename) {
		setBoard(board);
		setMovesLeft(movesLeft);
		setScore(score);
		setLevel(level);
		setSwapLeft(specialSwapLeft);
		setSavename(savename);
	}
	
	@SuppressWarnings("resource")
	public LokumGame(String fileName) throws IOException, InvalidLokumGameFileException {
		
		Lokum[][] board = null;
		int movesLeft = defaultMovesLeft;
		int swapLeft = -1;
		double score = -1;
		int level = defaultLevel;
		String savename = defaultSaveName;
		
		BufferedReader rd = new BufferedReader(new FileReader(fileName));
		
		String line;
		while ((line = rd.readLine()) != null) {
			line = line.trim();
			
			if (line.equals(""))
				continue;
			
			if(line.equalsIgnoreCase("board")) {
				line = rd.readLine();
				String[][] strBoard = new String[Integer.parseInt(LokumGG.conf.get("board-height"))][Integer.parseInt(LokumGG.conf.get("board-width"))];
				for(int y = 0; y < strBoard.length; y++){
					if (line == null) {
						throw new InvalidLokumGameFileException("Invalid board.");
					}
					String[] row = line.trim().split(" ");
					if (row.length != strBoard[y].length) {
						throw new InvalidLokumGameFileException("Invalid board.");
					}
					
					for (int i = 0; i < row.length; i++) {
						if(!Validator.validLokumText(row[i])){
							throw new InvalidLokumGameFileException("Invalid lokums on board.");
						}
					}
					
					strBoard[y] = row;
					line = rd.readLine();
				}
				board = Lokum.getBoardLokums(strBoard);
			}
			
			if(line.equals("movesleft")){
				line = rd.readLine();
				if (line == null) {
					throw new InvalidLokumGameFileException("Invalid number for movesleft.");
				}
				if (Validator.validNumber(line = line.trim())) {
					movesLeft = Integer.parseInt(line);
				}
			}
			
			if(line.equals("specialswapleft")){
				line = rd.readLine();
				if (line == null) {
					throw new InvalidLokumGameFileException("Invalid number for special swap left.");
				}
				if (Validator.validNumber(line = line.trim())) {
					swapLeft = Integer.parseInt(line);
				}
			}
			
			if(line.equals("score")){
				line = rd.readLine();
				if (line == null) {
					throw new InvalidLokumGameFileException("Invalid number for score.");
				}
				if (Validator.validNumber(line = line.trim())) {
					score = Double.parseDouble(line);
				}
			}
			
			if(line.equals("level")){
				line = rd.readLine();
				if (line == null) {
					throw new InvalidLokumGameFileException("Invalid number for level.");
				}
				if (Validator.validNumber(line = line.trim())) {
					level = Integer.parseInt(line);
				}
			}
			
			if(line.equals("savename")){
				line = rd.readLine();
				if (line == null) {
					throw new InvalidLokumGameFileException("Invalid filename for savename.");
				}
				if (Validator.validNumber(line = line.trim())) {
					savename = line;
				}
			}
		}
		
		if (board == null) {
			throw new InvalidLokumGameFileException("Board is required.");
		}
		
		setBoard(board);
		setMovesLeft(movesLeft);
		setScore(score);
		setLevel(level);
		setSwapLeft(swapLeft);
		setSavename(savename);
	}
	
	public String playerid() {
		return playerid;
	}
	
	public String playername() {
		return playername;
	}

	public Lokum[][] board() {
		return board;
	}

	public int moves() {
		return movesLeft;
	}

	private void setBoard(Lokum[][] board) {
		this.board = board;
	}

	private void setMovesLeft(int movesLeft) {
		this.movesLeft = (movesLeft != 0) ? movesLeft : defaultMovesLeft;
	}
	
	private void setSwapLeft(int swapLeft) {
		this.specialSwapLeft = (swapLeft != -1) ? swapLeft : defaultSpecialSwapLeft;
	}

	private void setScore(double score) {
		this.score = (score != -1) ? score : defaultScore;
	}

	private void setLevel(int level) {
		this.level = (level != 0) ? level : defaultLevel;
	}

	private void setSavename(String savename) {
		this.savename = savename;
	}

	public double score() {
		return score;
	}

	public int level() {
		return level;
	}
	
	public int specialSwap() {
		return specialSwapLeft;
	}
	
	public String savename() {
		return savename;
	}
	
	
}
